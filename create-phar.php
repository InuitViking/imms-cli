<?php

# PHAR name
$phar_name = 'imms.phar';
# Get the source directory, where all the code is
$srcRoot = __DIR__.'/src';
# Get the build directory, where we will put our finished archive
$buildRoot = __DIR__.'/build';
# Set the shebang
$shebang = "#!/usr/bin/php\n";

// Configuring the phar
$phar = new Phar($buildRoot . '/' . $phar_name,
	FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME, $phar_name);

// Build from directory
$phar->buildFromDirectory($srcRoot, '/.php$/');

# Default stub
$default_stub = $phar->createDefaultStub("index.php");

$stub = $shebang.$default_stub;
$phar->setStub($stub);
