<?php

if (\PHP_VERSION_ID < 80000) {
	echo "IMMS CLI requires PHP8.0 or newer\n";
	exit(1);
}

Phar::interceptFileFuncs();
//Phar::mount('imms-cms/', './');

$output = scandir('./config');
print_r($output);


if (file_exists(__DIR__.'/config/config.ini') && file_exists(__DIR__.'/vendor/autoload.php')) {
	require_once __DIR__.'/vendor/autoload.php';
} else {
	fwrite(STDERR, "IMMS was not found\n");
	exit(100);
}

use Angus\Imms\php\classes;

$version = '0.0.1';
$ini = parse_ini_file("../config/config.ini");

$intro = '
 ___  _____ ______   _____ ______   ________           ________  ___       ___
|\  \|\   _ \  _   \|\   _ \  _   \|\   ____\         |\   ____\|\  \     |\  \
\ \  \ \  \\\__\ \  \ \  \\\__\ \  \ \  \___|_        \ \  \___|\ \  \    \ \  \
 \ \  \ \  \\|__| \  \ \  \\|__| \  \ \_____  \        \ \  \    \ \  \    \ \  \
  \ \  \ \  \    \ \  \ \  \    \ \  \|____|\  \        \ \  \____\ \  \____\ \  \
   \ \__\ \__\    \ \__\ \__\    \ \__\____\_\  \        \ \_______\ \_______\ \__\
    \|__|\|__|     \|__|\|__|     \|__|\_________\        \|_______|\|_______|\|__|
                                      \|_________|

The CLI tool for the IMMS CMS.
Version: '.$version.'
============================
';

$shortOpts = "C:H::";
$shortOpts .= "qvcp";

$longOpts = [
	"clear-cache",
	"clear-single:",
	"pwgen",
	"hash::",
	"quiet",
	"version"
];

$options = getopt($shortOpts, $longOpts);

function printQuiet ($text, $q = null): void {
	if ($q != null) {
		if (isset($q['q']) || isset($q['quiet'])) {
			exit;
		} else {
			echo $text."\n";
		}
	}
}

if (!empty($options)) {
	if (isset($options['version']) || isset($options['v'])) {
		echo $intro;
	}

	if (isset($options['clear-cache']) || isset($options['c'])) {
		echo "Clearing cache";
		$cache = new classes\Cache();

		$cache->ClearCache(__DIR__ . '/..' . $ini['app_html_path']);
	}

	if (isset($options['clear-single']) || isset($options['C'])) {
		$path = '';

		if (isset($options['clear-single'])) {
			$path = $options['clear-single'];
		} elseif ($options['C']) {
			$path = $options['C'];
		}

		if ($path == '') {
			fwrite(STDERR, "This option requires the URL path of the page (not the full domain, just something as '/ideas/idea1')");
			exit(101);
		}

		echo "Clearing $path";
		$cache = new classes\Cache();

		$cache->ClearCacheSingularURL($path, true);
	}

	if (isset($options['hash']) || isset ($options['H'])) {
		$pw_string = '';

		if (!empty($options['hash'])) {
			$pw_string = $options['hash'];
		} elseif (!empty($options['H'])) {
			$pw_string = $options['H'];
		}

		if ($pw_string == '') {
			echo "Password: ";
			$fin = fopen("php://stdin","r");
			$pw_string = fgets($fin);
		}

		echo password_hash($pw_string, PASSWORD_DEFAULT)."\n";
	}

	if (isset($options['pwgen']) || isset($options['p'])) {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,.-^!#%&/()=?`@{[]}';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 16; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		echo implode($pass); //turn the array into a string
	}
}
